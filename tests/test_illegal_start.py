import numpy as np

from primawera.app import run_app


def test_illegal_shapes(capsys) -> None:
    data = np.random.random((3, 3, 3, 3, 3))
    assert not run_app(data=data)
    buffer = capsys.readouterr()
    assert buffer.out == "Creating app\n"
    assert buffer.err == f"Error: Data has invalid shape ({data.shape})!\n"

    data = np.random.random((3))
    assert not run_app(data=data)
    buffer = capsys.readouterr()
    assert buffer.out == "Creating app\n"
    assert buffer.err == f"Error: Data has invalid shape ({data.shape})!\n"


if __name__ == '__main__':
    test_illegal_shapes()
