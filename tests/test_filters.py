import numpy as np

import primawera.filters as filt


def test_log_sign():
    data = np.array([
        [10., 200., 3000.],
        [-10., 200., 3000.],
        [0.02, -1000., 1.],
    ])

    logs = np.log(np.abs(data) + 1)
    result = filt.logarithm_stretch(array=data, factor=1.0)
    assert np.all(np.sign(result) == np.sign(data))
    assert np.all(logs == np.abs(result))
