import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

a = np.linspace(0, 255, 256)
with open("grayscale.lut", "w") as out:
    for i, val in enumerate(a):
        val = int(val)
        out.write(f"{i};{val};{val};{val}\n")

b = np.array([a for _ in range(256)])
img1 = Image.fromarray(b)
plt.imshow(img1)
img1.save("gradient.tif")

c = b.copy()
c[:-50, :-50] = 3000
img2 = Image.fromarray(c)
plt.imshow(img2)
img2.save("gradient_with_peak.tif")
