import numpy as np

from primawera import app


def main() -> None:
    # You can pass in a numpy array
    data = np.random.rand(400, 400) * 100
    app.run_app(data=data.astype(np.int16), title="Primawera")

    # Even with overlay data
    overlay_data = np.random.rand(400, 500) * 40
    app.run_app(data=data.astype(np.int16), title="Primawera", overlay_data=overlay_data.astype(np.int8))

    # Or you can specify a path to a file
    app.run_app(data="images/obrazek.tif")



if __name__ == "__main__":
    main()
