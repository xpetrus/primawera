import numpy as np
from skimage import io as skio

from primawera.app import run_app


def test_modes() -> None:
    data = np.random.random((50, 50)) * 255
    data = data.astype(np.uint8)
    run_app(data, mode="grayscale")

    data = np.random.random((50, 50))
    data = data.astype(float)
    run_app(data, mode="float")

    data = np.random.random((10, 50, 50))
    data = data.astype(float)
    run_app(data, mode="float")

    data = np.random.random((50, 50))
    data = data.astype(complex)
    run_app(data, mode="complex")

    run_app("images/solosnica.jpg")
    img = skio.imread("images/solosnica.jpg")
    run_app(img, "color")


if __name__ == '__main__':
    test_modes()
