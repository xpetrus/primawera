import os

import h5py
import numpy as np
from PIL import Image
import scipy.fft as ft

def tiff_to_array(image):
    """
    Takes a Tiff image and returns the raw data as a numpy array.
    :param image: Tiff image (PIL).
    :return: Numpy array with data.
    """
    result = []
    for i in range(image.n_frames):
        image.seek(i)
        current_frame = np.array(image)
        result.append(current_frame)
    return np.array(result)


save_path = '//images/t21-float-3d.h5'
img_path = '//images/t21.tif'
print('image size: %d bytes' % os.path.getsize(img_path))
hf = h5py.File(save_path, 'w')  # open a hdf5 file

# varianta - float
img_np = tiff_to_array(Image.open(img_path)).astype(np.half)
# img_np = np.array(Image.open(img_path)).astype(np.half)
# skio.imshow(img_np)

# varianta - complex
# img_np = ft.fft2(np.array(Image.open(img_path)), norm="ortho")

dset = hf.create_dataset('Image', data=img_np, chunks=img_np.shape,
                         compression="gzip")  # write the data to hdf5 file
dset.attrs['CLASS'] = "IMAGE"
dset.attrs['IMAGE_VERSION'] = "1.2"

hf.close()  # close the hdf5 file
print('hdf5 file size: %d bytes' % os.path.getsize(save_path))
