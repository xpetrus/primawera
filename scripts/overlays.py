# %% Imports
import numpy as np

from primawera.app import run_app

# %% Interactive mode
#%gui qt5

# %% Generate data
data = np.random.random((10, 10)) * 255
data = data.astype(np.uint8)


# %% Generate overlays
overlay = np.zeros((10, 10), dtype=np.uint8)
overlay[:5, :5] = 255

# %% Visualise
run_app(data, "grayscale", overlay, True, "Overlays")

# %% Generate 3D Overlay data
overlay = np.zeros((10, 10, 10), dtype=np.uint8)
overlay[:5, :5, :5] = 255

data = np.random.random((10, 10, 10)) * 255
data = data.astype(np.uint8)

run_app(data, "grayscale", is_overlay_grayscale=True, overlay_data=overlay.astype(float))
# run_app(data, "grayscale", is_overlay_grayscale=True)

# %% Run
overlay = np.zeros((102, 290, 474, 3), dtype=np.uint8)
overlay[2, 10, 10] = (255, 255, 0)

run_app("./images/sequence/", overlay_data=overlay)

# %% Test various bitlength

# This should crash
overlay = np.zeros((102, 290, 474, 3), dtype=np.uint32)
overlay[2, 10, 10] = (255, 255, 0)

import unittest

class AppTestCase(unittest.TestCase):
    def test_failure_at_uint32(self):
        self.assertRaises(AssertionError, run_app("./images/sequence/", overlay_data=overlay))

# %% Test

overlay = np.zeros((50, 50, 50), dtype=np.uint32)
data = np.random.random((50, 50, 50))
overlay[:25, :25] = np.random.random(50) * 1000

run_app(data=overlay, overlay_data=overlay)
