# Documentation

The following document contains documentation of the Primawera source code and
its structure. It aims to provide a basic overview of terminology and the
project's structure.

## Basic Terminology
This section contains an enumeration of typical terms used in the project.

- Widget: Each application is split into widget objects. Those are derived from
    the `QWidget` object in the PyQT class. These widgets implement basic event
    handling and layouts and can nest various widgets inside them.
- Label: A widget containing text, HTML, or a single pixmap. Used
    for displaying the prepared pixmaps.
- Pixmap (`QPixmap`): An object in Qt framework representing a single off-screen
    image, which can be shown using a `QLabel` object on the screen.
- ScrollArea (`QScrollArea`): Provides an area for viewing an image. A scrollbar
    will be shown if the image is larger than the available space.
- Slots & Signals: These are used to pass data between various widgets in the Qt
    framework. A widget can have several slots through which it can accept data.
    On the other hand, a widget can emit a signal with some data. Then the Qt
    framework will propagate the data to all slots, which were connected to the
    given signal.
- `QAction`: An object containing an abstract view of some user-induced action
    , e.g., every clickable "thing" in the menu bar is an example of `QAction.` It is
    usually connected to some function, which is triggered, once the user clicks
    on the `QAction.`
- NDArray & ArrayLike:
- `Canvas`: Widget responsible for handling the visualizations, creating menubar
    , and handling the user input. There are various versions of the `Canvas` object
    for each view, for example, `ComplexCanvas` for viewing complex data.

## Overall structure of the code

The main window (`MainWindow`) is defined in the `app.py` file along with the
`run_app` function, which starts the application through code. This
file can also be run as the main script (that is utilized when the user invokes
`primawera` command in the terminal). This class is responsible for opening the
required files and transforming them into the correct shape (along with the
utility functions in `loading.py` file). Based on the nature of the data, a
particular `Canvas*` object is chosen.

If the data looks like 2D grayscale, colored, or floating point data the
`Canvas2D` canvas is chosen. If the data is, in fact, 3D, the `Canvas3D` object is
initialized, and finally, if the data contains complex data, the `CanvasComplex`
object is created. Each of these canvas objects defines its own layouts,
buttons, menus, etc., but all of them use the `Visualiser` object underneath.

The `Visualiser` object handles the visualization of integer NumPy arrays— with
a range from 0 to 255— and overlaying integer-based arrays on top. Internally, the
data is transformed into `QPixmap` objects so that the Qt framework can handle the
visualization using `PixmapLabel` object. Since the images can have various sizes, the
`Visualiser` uses a custom widget called `HighlightScrollArea` to nest the
`PixmapLabel` inside a `QScrollArea` widget. When overlaying data, 0 inside the
overlay data indicates transparent background. The data can be both grayscale
(in this case LUTs can be applied) or colored.

![Class diagram](diagrams/object.png)

In the above image, the class diagram of the project is present. The main event
loop is handled in the `MainWindow` objects. After the data is selected and
transformed into a suitable format, a single subclass of the `CanvasBase` object
is initialized. It handles the `InformationWindow` object, creates the
`PreviewWindow`, and has multiple `Visualiser` objects depending on the data
type.

Before the data can be shown, the canvas objects have to transform the data into
`QPixmap` objects which are then passed to `PixmapLabel` to show the results.

As for the signals and slots, they fulfill two primary purposes:
1) When the user selects a filter, which requires the `PreviewWindow` window,
the chosen values for parameters are passed to the `CanvasBase` object using the
`return_parameters` signal. 
2) When the user is hovering above the image, the information in the
`InformationWindow` must be updated. To facilitate this change, the
`InformationWindow` sends a signal with relative coordinates to the
`Visualiser`. It calculates the real coordinates (some canvases can have
multiple views; therefore, the real coordinates cannot be calculated in the
`PixmapLabel` object.) This information is emitted to the `Canvas*` object,
which will combine the position information with the mapped and original value
at that position, and together is sent to the `InformationWindow` to be
displayed for the user.

## Data representation

All canvas objects use the same underlying image format; all data is transformed
to (frames, width, height) and RGB data to (frames, width, height, channels).
The canvas object already expects the data to be in this format, so other parts
of the code must check this.

Another nontrivial concept is the image modes. I recommend looking at [docs](https://pillow.readthedocs.io/en/stable/handbook/concepts.html),
to get a broad idea of their meaning. In general, the modes used by the PIL
library might not be the same as you get when using libtiff to open TIFF files;
therefore, the application sometimes might try to make an educated guess, but it
is generally safer to exit the application. In such cases, the user can provide
the mode manually to clear up the confusion.

## Extending code

If you want to create a new view, the general idea is to create a new `CanvasBase`
subclass. This new subclass can define its own layout, number of filters, and
custom menubars, but it has to implement all the methods from `CanvasBase`
containg `NotImplementedError`.

To communicate with the main application, each new Canvas has to provide a list
of `QActions,` menubars and implement the `_connect_info_window_and_canvas`
method, which connects the canvas with the `InformationWindow.` That is needed
for the information panel to show the correct cursor location and the pixel's
value. The rest should be handled by the `CanvasBase` class.

### Menu bars, filters, and LUTs.

The main menubar is split into three parts. The first part consists of options
that are shared among all visualization options and are handled by the
`MainWindow` object. The various menus which are part of the particular canvas
object follow. Those might be different filters, views, LUTs, etc. Finally, the
last part consists of other general menu entries, such as the help menu, and is
added by the `MainWindow` object.

To define which menus the canvas offers, the class must return a list of menus
in the `get_menus` method as a list of (string, List[QAction]) tuples. The
string contains the name of the submenu and the list of actions.

The implementation of custom filters and LUTs works the same way. In principle,
the main window does not care about which filters you implement as long you
provide the relevant QAction objects through the `get_actions` and `get_menus`
methods. But it is the canvas's responsibility to apply and interpret
such actions. Some standard filters are already prepared in the `filters.py` file.

Every canvas should support the information panel, which means you have to
connect `Visualiser`'s `value_change_emitter_(vertical, horizontal)` signals
to the `InformationWindow` as can be seen in `Canvas2D`. This step is necessary
because the `CanvasBase` object does not know how many `Visualiser` objects does
the canvas object has.

## Note on typing

The code is quite inconsistent regarding typing.
The reason is that during development, typing was dropped due to the added
headaches caused by NumPy. It contains two main types - NDArray and ArrayLike,
but the problem is that the type cannot reflect the number of dimensions
of the data. Moreover, these types might be constructed dynamically by NumPy,
leading to typing errors (NDArray[int] != NDArray[some alias for int]);
therefore, the typing was dropped, and instead, docstrings are used.
