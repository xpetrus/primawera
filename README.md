# Intro

Primawera is a simple imager viewer with support for non-standard image data such as complex, floating point
or 3D image data. For more detailed description about the basics of the viewer, please view the [official website](https://cbia.fi.muni.cz/software/primawera).

# Installation

This package is available at [PyPI](https://pypi.org/project/primawera/). To install the package using `pip`
run:
```bash
python -m pip install primawera
```

# Building

## Required packages

More detailed information about version of packages is inside the `pyproject.toml` file.

- Python 3.11
- numpy 2
- PIL
- PyQt5
- Qt 5
- h5py

## Building and installing the package

Make sure you have the latest version of `setuptools`, `pip` and `venv` packages as the building requires some
newer features of `setuptools`.

Inside the root folder run:
```bash
python -m build
```

The command should build a wheel file inside the `dist` folder. To install it run:
```bash
pip install <PATH TO WHEEL FILE>
```

# Usage

## Open empty window

It is possible to run the viewer without any data.
```python
from primawera.app import create_window
create_window()
```

## Visualise numpy data

If you want to visualise data inside a numpy array, you will have to import the function `run_app`. Sometimes it is not
possible to automatically infer the mode (see [Pillow image modes](https://pillow.readthedocs.io/en/stable/handbook/concepts.html)).
in which case it has to be provided manually.
```python
from primawera.app import run_app
import numpy as np
data = np.random.random((10, 10, 10))
run_app(data)
# Or
run_app(data, mode="float")

# You can also add overlay data
overlay_data = np.random.random(data.shape)  # same shape as data
run_app(data, mode="float", overlay_data=overlay_data)
```

## Run from terminal

Simply run

```bash
primawera
```

## Recognized image modes (case insensitive)

| Image type | Legal Mode                                   |
|------------|----------------------------------------------|
| Boolean    | 1, bool                                      |
| Grayscale  | gray, grey, grayscale, greyscale, I;16 I;16B |
| RGB        | rgb, color, colour                           |
| Floating   | f, float, floating                           |
| Complex    | c, complex                                   |                                            
