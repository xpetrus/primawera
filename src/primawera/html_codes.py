about_page_content = """
<html>
    <head>
        <style>
            body {
                    margin: auto;
                    padding: 10px;
                }
            h1 {text-align: center;}
        </style>
    </head>
    <body>
        <h1>
            Primawera
        </h1>
        <p>Primawera is a simple image viewer with 3D, complex and floating
         images support.</p>
        <p>It is developed and maintained by Bruno Petrus at Faculty of
         Informatics of Masaryk University.</p>
        <p>Licensed using GPLv3.</p>
    </body>
</html>
"""

command_page_content = """
<html>
    <head>
        <style>
            ul {list-style-type: none;}
            body {
                    margin: auto;
                    padding: 10px;
                }
            h1 {text-align: center;}
        </style>
    </head>
<body>
    <h1>Scaling</h1>
        <ul>
            <li><strong>&#60;+&#62;</strong> To scale up the image by factor
             of 2</li>
            <li><strong>&#60;-&#62;</strong> To scale down the image by factor
             of 2</li>
             <li><strong>&#60;i&#62;</strong> To show or hide the information
             panel</li>
             <li><strong>&#60;SPACE&#62;</strong> To hide the overlay</li>
        </ul>
    <h1>Changing frames inside 3D view</h1>
    <ul>
        <li><strong>&#60;n&#62;</strong> Switch to the next frame</li>
        <li><strong>&#60;p&#62;</strong> Switch to the previous frame</li>
    </ul>
</body>
</html>
"""
